clone from git use : <br>
create file name .env in app/app <br>
copy data from .env.example into .env <br>
login database create database<br>
docker exec -it projectdbms-web /bin/bash<br>
composer update <br>
php artisan key:generate <br>
php artisan migrate:refresh --seed <br>
docker exec -it projectdbms-db /bin/bash <br>
cd <br>
mysql -u root -p <br>
use equipments <br>
source ChangeRoom.sql <br>
source CheckEquipment.sql <br>
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class EquipmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Equipments')->insert([
            [
                'eq_serialnumber' => 'COIN01',
                'eq_name' => 'computer',
                'eq_brand' => 'INTEL',
                'eq_status' => 'available',
                'import_date' => Carbon::now()->toDateString(),
                'export_date' => null,
                'comp_id' => 'COMP01',
                'dep_id' => 'IT',
                'room_id' => 'IB11M201',
                'create_by' => 'Pakawat W',
                'update_by' => 'Pakawat W',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            [
                'eq_serialnumber' => 'CH01',
                'eq_name' => 'Chair',
                'eq_brand' => '',
                'eq_status' => 'available',
                'import_date' => Carbon::now()->toDateString(),
                'export_date' => Carbon::now()->addday(7)->toDateString(),
                'comp_id' => 'COMP02',
                'dep_id' => 'HR',
                'room_id' => 'HB11M202',
                'create_by' => 'Tanakorn P',
                'update_by' => 'Tanakorn P',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'eq_serialnumber' => 'CH02',
                'eq_name' => 'Chair',
                'eq_brand' => '',
                'eq_status' => 'Lost',
                'import_date' => Carbon::now()->toDateString(),
                'export_date' => Carbon::now()->addday(7)->toDateString(),
                'comp_id' => 'COMP02',
                'dep_id' => 'HR',
                'room_id' => 'IB11M201',
                'create_by' => 'Tanakorn P',
                'update_by' => 'Tanakorn P',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'eq_serialnumber' => 'CH03',
                'eq_name' => 'Chair',
                'eq_brand' => '',
                'eq_status' => 'repair',
                'import_date' => Carbon::now()->toDateString(),
                'export_date' => Carbon::now()->addday(7)->toDateString(),
                'comp_id' => 'COMP02',
                'dep_id' => 'HR',
                'room_id' => 'IB11M201',
                'create_by' => 'Tanakorn P',
                'update_by' => 'Tanakorn P',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'eq_serialnumber' => 'CH04',
                'eq_name' => 'Chair',
                'eq_brand' => '',
                'eq_status' => 'available',
                'import_date' => Carbon::now()->toDateString(),
                'export_date' => Carbon::now()->addday(7)->toDateString(),
                'comp_id' => 'COMP02',
                'dep_id' => 'HR',
                'room_id' => 'HB11M202',
                'create_by' => 'Tanakorn P',
                'update_by' => 'Tanakorn P',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);//
    }
}

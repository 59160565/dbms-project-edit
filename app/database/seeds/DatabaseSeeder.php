<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        EquipmentsSeeder::class,
        UsersSeeder::class,
        RoomsTableSeeder::class,
        DepartmentsTableSeeder::class,
        CompanysTableSeeder::class,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [   'name' => 'Pakawat W',
                'email' => '59161106@go.buu.ac.th',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'Tanakorn P',
                'email' => '59160565@go.buu.ac.th',
                'password' => bcrypt('12345678')
            ]
        ]);
    }
}

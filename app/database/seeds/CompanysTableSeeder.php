<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class CompanysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Companys')->insert([
            [
                'comp_id' => 'COMP01',
                'comp_name' => 'Siga',
                'comp_addr' => '123/123',
                'create_by' => 'Pakawat W',
                'update_by' => 'Pakawat W',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'comp_id' => 'COMP02',
                'comp_name' => 'Intel',
                'comp_addr' => '124/123',
                'create_by' => 'Pakawat W',
                'update_by' => 'Pakawat W',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}

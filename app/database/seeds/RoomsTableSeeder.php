<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Rooms')->insert([
            [
                'room_id' => 'IB11M201',
                'room_name' => 'M201',
                'create_by' => 'Pakawat W',
                'update_by' => 'Pakawat W',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'room_id' => 'HB11M202',
                'room_name' => 'M202',
                'create_by' => 'Pakawat W',
                'update_by' => 'Pakawat W',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}

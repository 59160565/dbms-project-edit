<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Departments')->insert([
            [
                'dep_id' => 'IT',
                'dep_name' => 'Information Technology',
                'create_by' => 'Pakawat W',
                'update_by' => 'Pakawat W',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'dep_id' => 'HR',
                'dep_name' => 'Human Resource',
                'create_by' => 'Pakawat W',
                'update_by' => 'Pakawat W',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);//
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableChangeRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ChangeRooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->collation = 'utf8_general_ci';
            $table->string('change_id', 100)->collation('utf8_general_ci');
            $table->string('changeeq_serialnumber', 100)->collation('utf8_general_ci');
            $table->string('oldroom_id', 100)->collation('utf8_general_ci');
            $table->string('newroom_id', 100)->collation('utf8_general_ci');
            $table->string('create_by', 100)->collation('utf8_general_ci');
            $table->string('update_by', 100)->collation('utf8_general_ci');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ChangeRooms');
    }
}

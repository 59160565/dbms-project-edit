<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEquipment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Equipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->collation = 'utf8_general_ci';
            $table->string('eq_serialnumber', 100)->collation('utf8_general_ci');
            $table->string('eq_name', 100)->collation('utf8_general_ci');
            $table->string('eq_brand', 100)->nullable(true)->collation('utf8_general_ci');
            $table->enum('eq_status', ['available', 'repair', 'lost'])->collation('utf8_general_ci');
            $table->date('import_date');
            $table->date('export_date')->nullable(true);
            $table->string('comp_id', 100)->collation('utf8_general_ci');
            $table->string('dep_id', 100)->collation('utf8_general_ci');
            $table->string('room_id', 100)->collation('utf8_general_ci')->nullable(true);
            $table->string('create_by', 100)->collation('utf8_general_ci');
            $table->string('update_by', 100)->collation('utf8_general_ci');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Equipments');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRepairEquipments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RepairEquipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->collation = 'utf8_general_ci';	
            $table->string('repair_id', 100)->collation('utf8_general_ci');
            $table->string('repaircheck_id', 100)->collation('utf8_general_ci');
            $table->string('repaireq_serialnumber', 100)->collation('utf8_general_ci');
            $table->string('create_by', 100)->collation('utf8_general_ci');
            $table->string('update_by', 100)->collation('utf8_general_ci');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RepairEquipments');
    }
}

@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
  <a href="{{ route('home') }}">Home</a><br>
  <a href="{{ route('equipments.create') }}">New equipment</a>       <a href="{{ route('rooms.create') }}">New Room</a><br>
  <a href="{{ route('departments.create') }}">New department</a>      <a href="{{ route('companys.create') }}">New company</a><br>
  <table class="table table-striped table-bordered">
    <thead>
        <tr class="bg-primary text-white">
            <th>Name</th>
            <th>Status</th>
            <th>Serialnumber</th>
            <th>Room</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($equipments as $equipment)
        <tr>
            <td>{{ $equipment->eq_name }}</td>
            <td>{{ $equipment->eq_status }}</td>
            <td>{{ $equipment->eq_serialnumber }}</td>
            <td>{{ $equipment->room_id }}</td>
            <td>
              <a type="button" class="btn btn-info" data-toggle="modal" data-target="#yourModal{{$equipment->id}}"> View </a> | <a href="{{ route('changerooms.show',$equipment->id) }}" class="btn btn-success" method="get">Change room </a> | 
              <a href="{{ route('equipments.edit',$equipment->id) }}" class="btn btn-primary">Edit </a> | <a href="{{ route('equipments.show',$equipment->id) }}" class="btn btn-success" method="get">Change status </a>
              <form action="{{ route('equipments.destroy',$equipment->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@foreach ($equipments as $e)    
    <div class="modal fade" id="yourModal{{$e->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">{{$e->eq_name}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <table class="table table-hover">
              <tbody>
                <tr>
                  <td>serialnumber</td>
                  <td>{{$e->eq_serialnumber}}</td>
                </tr>
                <tr>
                  <td>brand</td>
                  <td>{{$e->eq_brand}}</td>
                </tr>
                <tr>
                  <td>status</td>
                  <td>{{$e->eq_status}}</td>
                </tr>
                <tr>
                  <td>import_date</td>
                  <td>{{$e->import_date}}</td>
                </tr>
                <tr>
                  <td>export_date</td>
                  <td>{{$e->export_date}}</td>
                </tr>
                <tr>
                  <td>comp_name</td>
                  <td>{{$e->comp_name}}</td>
                </tr>
                <tr>
                  <td>dep_name</td>
                  <td>{{$e->dep_name}}</td>
                </tr>
                <tr>
                  <td>room_name</td>
                  <td>{{$e->room_name}}</td>
                </tr>
                <tr>
                  <td>create_by</td>
                  <td>{{$e->create_by}}</td>
                </tr>
                <tr>
                  <td>update_by</td>
                  <td>{{$e->update_by}}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
<!-- Modal: modalCart -->
@endforeach
@endguest
@endsection

@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form action="{{ route('equipments.update',$equipment->id) }}" method="post">
                @method('PUT')
                @csrf
                    <table class="table" border=1>
                        <tbody>
                            <tr>
                                <td>รหัส </td>
                                <td><input type="text" name="eq_serialnumber" value="{{ $equipment->eq_serialnumber }}" readonly> </td>
                            </tr>
                            <tr>
                            <tr>
                                <td>ชื่อวัสดุ </td>
                                <td><input type="text" name="eq_name" value="{{ $equipment->eq_name }}" require> </td>
                            </tr>
                            <tr>
                                <td>ยี่ห้อ </td>
                                <td><input type="text" name="eq_brand" value="{{ $equipment->eq_brand }}" require> </td>
                            </tr>

                            <tr>
                                <td>export_date </td>
                                <td><input type="date" name="export_date" value="{{ $equipment->export_date }}" require> </td>
                            </tr>
                            <tr>
                                <td>company </td>
                                <td>
                                <select name="comp_id">
                                @foreach ($companys as $company)
                                    <option value="{{ $company->comp_id }}">{{$company->comp_name}}</option>
                                @endforeach
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td>department </td>
                                <td>
                                <select name="dep_id">
                                @foreach ($departments as $department)
                                    <option value="{{ $department->dep_id }}">{{$department->dep_name}}</option>
                                @endforeach
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="edit">
                                    <a class="btn btn-default btn-close" href="{{ URL::previous() }}">Cancel</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
@endguest
@endsection

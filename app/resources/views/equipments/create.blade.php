@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form action="{{ route('equipments.store') }}" method="post">
                @csrf
                    <table class="table" border=1>
                        <tbody>
                            <tr>
                                <td>serialnumber </td>
                                <td><input type="text" name="eq_serialnumber" require> </td>
                            </tr>
                            <tr>
                                <td>ชื่อวัสดุ </td>
                                <td><input type="text" name="eq_name" require> </td>
                            </tr>
                            <tr>
                                <td>ยี่ห้อ </td>
                                <td><input type="text" name="eq_brand" require> </td>
                            </tr>
                            <tr>
                                <td>วันหมดอายุ </td>
                                <td><input type="date" name="export_date" require> </td>
                            </tr>
                            <tr>
                                <td>บริษัท </td>
                                <td><select name="comp_id">
                                @foreach ($companys as $company)
                                    <option value="{{ $company->comp_id }}">{{$company->comp_name}}</option>
                                @endforeach
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td>หน่วยงาน </td>
                                <td><select name="dep_id">
                                @foreach ($departments as $department)
                                    <option value="{{ $department->dep_id }}">{{$department->dep_name}}</option>
                                @endforeach
                                </select></td>
                            </tr>
                            <tr>
                                <td>ห้อง </td>
                                <td><select name="room_id">
                                @foreach ($rooms as $room)
                                    <option value="{{ $room->room_id }}">{{$room->room_name}}</option>
                                @endforeach
                                </select></td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="เพิ่มข้อมูล">
                                    <a class="btn btn-default btn-close" href="{{ URL::previous() }}">Cancel</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
@endguest
@endsection

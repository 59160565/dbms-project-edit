@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! <br> <br>

                    <a href="{{ route('equipments.index') }}">Show Equipment</a> <br>
                    <a href="{{ route('rooms.index') }}">Show Room</a> <br>
                    <a href="{{ route('checkequipments.index') }}">Show Check</a> <br>
                    <a href="{{ route('departments.index') }}">Show Department</a> <br>
                    <a href="{{ route('companys.index') }}">Show Company</a> <br>
                    <a href="{{ route('lostequipments.index') }}">Show Lostequipment</a> <br>
                    <a href="{{ route('repairequipments.index') }}">Show Repairequipment</a> <br>
                    <a href="{{ route('changerooms.index') }}">Show Changeroom</a> <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

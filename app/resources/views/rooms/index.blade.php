@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<a href="{{ route('home') }}">Home</a><br>
  <table class="table table-striped table-bordered">
    <thead>
        <tr class="bg-primary text-white">
            <th>room_id</th>
            <th>room_name</th>
            <th>create_by</th>
            <th>update_by</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($rooms as $room)
        <tr>
            <td>{{ $room->room_id }}</td>
            <td>{{ $room->room_name }}</td>
            <td>{{ $room->create_by }}</td>
            <td>{{ $room->update_by }}</td>
            <td>{{ $room->created_at }}</td>
            <td>{{ $room->updated_at }}</td>
            <td>
              <a href="{{ route('rooms.edit',$room->id) }}" class="btn btn-primary">Edit </a>
              <form action="{{ route('rooms.destroy',$room->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-default btn-close" href="{{ URL::previous() }}">Back</a>
@endguest
@endsection
@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<a href="{{ route('home') }}">Home</a><br>
  <table class="table table-striped table-bordered">
    <thead>
        <tr class="bg-primary text-white">
            <th>dep_id</th>
            <th>dep_name</th>
            <th>create_by</th>
            <th>update_by</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($departments as $department)
        <tr>
            <td>{{ $department->dep_id }}</td>
            <td>{{ $department->dep_name }}</td>
            <td>{{ $department->create_by }}</td>
            <td>{{ $department->update_by }}</td>
            <td>{{ $department->created_at }}</td>
            <td>{{ $department->updated_at }}</td>
            <td>
              <a href="{{ route('departments.edit',$department->id) }}" class="btn btn-primary">Edit </a>
              <form action="{{ route('departments.destroy',$department->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-default btn-close" href="{{ URL::previous() }}">Back</a>
@endguest
@endsection
@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<a href="{{ route('home') }}">Home</a><br>
  <table class="table table-striped table-bordered">
    <thead>
        <tr class="bg-primary text-white">
            <th>#</th>
            <th>Equipment Name</th>
            <th>check_by</th>
            <th>check_date</th>
            <th>check_status</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($checkequipments as $checkequipment)
        <tr>
            <td>{{ $checkequipment->check_id }}</td>
            <td>{{ $checkequipment->checkeq_serialnumber }}</td>
            <td>{{ $checkequipment->create_by }}</td>
            <td>{{ $checkequipment->created_at }}</td>
            <td>{{ $checkequipment->check_status }}</td>
            <td>
              <form action="{{ route('checkequipments.destroy',$checkequipment->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-default btn-close" href="{{ URL::previous() }}">Back</a>
@endguest
@endsection
@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form action="{{ route('checkequipments.store') }}" method="post">
                @csrf
                    <table class="table" border=1>
                    <tbody>
                      <tr>
                        <td>check_id</td>
                        <td><input type="text" name="check_id"></input></td>
                      </tr>
                      <tr>
                        <td>eq_serialnumber</td>
                        <td><input type="text" name="checkeq_serialnumber" value="{{$equipments->eq_serialnumber}}" readonly></input></td>
                      </tr>
                      <tr>
                        <td>name</td>
                        <td>{{$equipments->eq_name}}</td>
                      </tr>
                      <tr>
                        <td>room</td>
                        <td>{{$equipments->room_id}}</td>
                      </tr>
                      <tr>
                        <td>status</td>
                        <td><select name="check_status">
                          <option selected>Choose...</option>
                          <option value="available">available</option>
                          <option value="repair">repair</option>
                          <option value="lost">lost</option>
                        </select></td>
                      </tr>
                      <tr>
                        <td>lost/repair id</td>
                        <td><input type="text" name="lr_id"></input></td>
                      </tr>
                      <tr>
                                <td>
                                    <input type="submit" value="เพิ่มข้อมูล">
                                    <a class="btn btn-default btn-close" href="{{ URL::previous() }}">Cancel</a>
                                </td>
                            </tr>
                    </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
@endguest
@endsection
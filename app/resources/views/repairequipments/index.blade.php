@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<a href="{{ route('home') }}">Home</a><br>
  <table class="table table-striped table-bordered">
    <thead>
        <tr class="bg-primary text-white">
            <th>repair_id</th>
            <th>repaircheck_id</th>
            <th>repaireq_serailnumber</th>
            <th>create_by</th>
            <th>update_by</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($repairequipments as $repairequipment)
        <tr>
            <td>{{ $repairequipment->repair_id }}</td>
            <td>{{ $repairequipment->repaircheck_id }}</td>
            <td>{{ $repairequipment->repaireq_serialnumber }}</td>
            <td>{{ $repairequipment->create_by }}</td>
            <td>{{ $repairequipment->update_by }}</td>
            <td>{{ $repairequipment->created_at }}</td>
            <td>{{ $repairequipment->updated_at }}</td>
            <td>
              <form action="{{ route('repairequipments.destroy',$repairequipment->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-default btn-close" href="{{ URL::previous() }}">Back</a>
@endguest
@endsection
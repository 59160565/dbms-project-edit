@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form action="{{ route('changerooms.store') }}" method="post">
                @csrf
                    <table class="table" border=1>
                        <tbody>
                            <tr>
                                <td>change_id</td>
                                <td><input type="text" name="change_id"> </td>
                            </tr>
                            <tr>
                                <td>changeeq_serialnumber</td>
                                <td><input type="text" name="changeeq_serialnumber" value="{{ $equipments->eq_serialnumber }}" readonly> </td>
                            </tr>
                            <tr>
                                <td>oldroom_id</td>
                                <td><input type="text" name="oldroom_id" value="{{ $equipments->room_id }}" readonly> </td>
                            </tr>
                            <tr>
                                <td>newroom_id</td>
                                <td>
                                <select name="newroom_id">
                                @foreach ($rooms as $room)
                                    <option value="{{ $room->room_id }}">{{$room->room_id}}</option>
                                @endforeach
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="เพิ่มข้อมูล">
                                    <a class="btn btn-default btn-close" href="{{ URL::previous() }}">Cancel</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
@endguest
@endsection

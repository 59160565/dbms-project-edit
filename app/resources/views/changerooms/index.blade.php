@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<a href="{{ route('home') }}">Home</a><br>
  <table class="table table-striped table-bordered">
    <thead>
        <tr class="bg-primary text-white">
            <th>change_id</th>
            <th>changeeq_serialnumber</th>
            <th>oldroom_id</th>
            <th>newroom_id</th>
            <th>create_by</th>
            <th>update_by</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($changerooms as $changeroom)
        <tr>
            <td>{{ $changeroom->change_id }}</td>
            <td>{{ $changeroom->changeeq_serialnumber }}</td>
            <td>{{ $changeroom->oldroom_id }}</td>
            <td>{{ $changeroom->newroom_id }}</td>
            <td>{{ $changeroom->create_by }}</td>
            <td>{{ $changeroom->update_by }}</td>
            <td>{{ $changeroom->created_at }}</td>
            <td>{{ $changeroom->updated_at }}</td>
            <td>
              <form action="{{ route('changerooms.destroy',$changeroom->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-default btn-close" href="{{ URL::previous() }}">Back</a>
@endguest
@endsection
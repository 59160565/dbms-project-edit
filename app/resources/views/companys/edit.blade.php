@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form action="{{ route('companys.update',$companys->id) }}" method="post">
                @method('PUT')
                @csrf
                    <table class="table" border=1>
                        <tbody>
                            <tr>
                                <td>รหัสบริษัท </td>
                                <td><input type="text" name="comp_id" value="{{ $companys->comp_id }}" require> </td>
                            </tr>
                            <tr>
                            <tr>
                                <td>ชื่อบริษัท </td>
                                <td><input type="text" name="comp_name" value="{{ $companys->comp_name }}" require> </td>
                            </tr>
                            <tr>
                                <td>ที่อยู่ </td>
                                <td><input type="text" name="comp_addr" value="{{ $companys->comp_addr }}" require> </td>
                            </tr>

                            
                            <tr>
                                <td>
                                    <input type="submit" value="edit">
                                    <a class="btn btn-default btn-close" href="{{ URL::previous() }}">Cancel</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
@endguest
@endsection

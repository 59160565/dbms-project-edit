@extends('layouts.app')

@section('content')

@guest
 <h1>You need to login</h1>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
  </li>
  @if (Route::has('register'))
    <li class="nav-item">
      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
  @endif
@else
<a href="{{ route('home') }}">Home</a><br>
  <table class="table table-striped table-bordered">
    <thead>
        <tr class="bg-primary text-white">
            <th>lost_id</th>
            <th>lostcheck_id</th>
            <th>losteq_serialnumber</th>
            <th>create_by</th>
            <th>update_by</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($lostequipments as $lostequipment)
        <tr>
            <td>{{ $lostequipment->lost_id }}</td>
            <td>{{ $lostequipment->lostcheck_id }}</td>
            <td>{{ $lostequipment->losteq_serialnumber }}</td>
            <td>{{ $lostequipment->create_by }}</td>
            <td>{{ $lostequipment->update_by }}</td>
            <td>{{ $lostequipment->created_at }}</td>
            <td>{{ $lostequipment->updated_at }}</td>
            <td>
              <form action="{{ route('lostequipments.destroy',$lostequipment->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-default btn-close" href="{{ URL::previous() }}">Back</a>
@endguest
@endsection
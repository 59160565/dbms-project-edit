<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LostEquipmentsModel extends Model
{
    protected $table="LostEquipments";
    protected $fillable= [
        'lost_id',
        'lostcheck_id',
        'losteq_serailnumber',
        'create_by',
        'update_by',
        'created_at',
        'updated_at'
    ];
}

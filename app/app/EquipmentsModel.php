<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentsModel extends Model
{
    protected $table="Equipments";
    protected $fillable= [
        'eq_name',
        'eq_brand',
        'eq_serialnumber',
        'eq_status',
        'import_date',
        'export_date',
        'comp_id',
        'dep_id',
        'room_id',
        'created_at',
        'updated_at',
        'create_by',
        'update_by'
    ];//

}

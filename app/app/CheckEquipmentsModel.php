<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckEquipmentsModel extends Model
{
    protected $table="CheckEquipments";

    protected $fillable= [
        'check_id',
        'checkeq_serialnumber',
        'check_status',
        'lr_id',
        'create_by',
        'update_by',
        'created_at',
        'updated_at'
    ];
}

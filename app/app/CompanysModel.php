<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanysModel extends Model
{
    protected $table="Companys";
    protected $fillable= [
        'comp_id',
        'comp_name',
        'comp_addr',
        'create_by',
        'update_by',
        'created_at',
        'updated_at'
    ];
}

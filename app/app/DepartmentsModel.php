<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentsModel extends Model
{
    protected $table="Departments";
    protected $fillable= [
        'dep_id',
        'dep_name',
        'create_by',
        'update_by',
        'created_at',
        'updated_at'
    ];
}

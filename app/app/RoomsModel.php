<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomsModel extends Model
{
    protected $table="Rooms";

    protected $fillable= [
        'room_id',
        'room_name',
        'create_at',
        'update_at',
        'created_by',
        'updated_by'
    ];//

    
}

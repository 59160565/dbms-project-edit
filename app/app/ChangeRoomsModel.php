<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeRoomsModel extends Model
{
    protected $table="ChangeRooms";

    protected $fillable= [
        'change_id',
        'changeeq_serialnumber',
        'changeoldroom_id',
        'changenewroom_id',
        'create_by',
        'update_by',
        'created_at',
        'updated_at'
    ];
}

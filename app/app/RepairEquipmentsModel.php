<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepairEquipmentsModel extends Model
{
    protected $table="RepairEquipments";
    protected $fillable= [
        'repair_id',
        'repaircheck_id',
        'repaireq_serailnumber',
        'create_by',
        'update_by',
        'created_at',
        'updated_at'
    ];
}

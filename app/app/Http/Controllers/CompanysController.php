<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\CompanysModel;
use Carbon\Carbon;

class CompanysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companys = DB::table('Companys')->get();
        return view('companys.index', ['companys'=>$companys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            DB::table("Companys")->insert([
                'comp_id'=>$request->comp_id,
                'comp_name'=>$request->comp_name,
                'comp_addr'=>$request->comp_addr,
                'create_by'=>$request->user()->name,
                'update_by'=>$request->user()->name,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]);

        } catch(ValidationException $e) {
            DB::rollback();
        }

        DB::commit();

    return redirect('companys');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companys = CompanysModel::find($id);
        return view('companys.create', ['companys'=>$companys]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companys = CompanysModel::find($id);

        return view('companys.edit', compact('companys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'comp_addr'=> 'required',
            'comp_id'=> 'required',
            'comp_name'=> 'required',
        ]);

        $company = CompanysModel::find($id);

        $company->comp_id = $request->get('comp_id');
        $company->comp_name = $request->get('comp_name');
        $company->comp_addr = $request->get('comp_addr');
        $company->update_by = $request->user()->name;
        $company->save();
        return redirect('companys');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = CompanysModel::find($id);

        $company->delete();
        return redirect('companys');
    }
}

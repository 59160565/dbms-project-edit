<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DepartmentsModel;
use Carbon\Carbon;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = DB::table('Departments')->get();
        return view('departments.index', ['departments'=>$departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            DB::table("Departments")->insert([
                'dep_id'=>$request->dep_id,
                'dep_name'=>$request->dep_name,
                'create_by'=>$request->user()->name,
                'update_by'=>$request->user()->name,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]);

        } catch(ValidationException $e) {
            DB::rollback();
        }

        DB::commit();

    return redirect('departments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departments = DepartmentsModel::find($id);
        return view('departments.create', ['departments'=>$departments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = DepartmentsModel::find($id);

        return view('departments.edit', compact('departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'dep_name'=> 'required',
            'dep_id'=> 'required',
        ]);

        $department = DepartmentsModel::find($id);

        $department->dep_id = $request->get('dep_id');
        $department->dep_name = $request->get('dep_name');
        $department->update_by = $request->user()->name;
        $department->save();
        return redirect('departments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = DepartmentsModel::find($id);

        $department->delete();
        return redirect('departments');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\EquipmentsModel;
use App\CheckEquipmentsModel;
use Carbon\Carbon;

class CheckEquipmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $checkequipments = DB::table('CheckEquipments')->get();
        return view('checkequipments.index', ['checkequipments'=>$checkequipments]);//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            DB::table("CheckEquipments")->insert([
                'check_id'=>$request->check_id,
                'lr_id'=>$request->lr_id,
                'checkeq_serialnumber'=>$request->checkeq_serialnumber,
                'check_status'=>$request->check_status,
                'create_by'=>$request->user()->name,
                'update_by'=>$request->user()->name,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]);

        } catch(ValidationException $e) {
            DB::rollback();
        }

        DB::commit();

    return redirect('equipments');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $equipments = EquipmentsModel::find($id);
        return view('checkequipments.create', ['equipments'=>$equipments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkequipment = CheckEquipmentsModel::find($id);

        $checkequipment->delete();
        return redirect('checkequipments');//
    }
}

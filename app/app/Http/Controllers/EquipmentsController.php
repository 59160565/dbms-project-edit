<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\EquipmentsModel;
use App\DepartmentsModel;
use App\CompanysModel;
use App\RoomsModel;
use Carbon\Carbon;


class EquipmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $equipments = DB::table('Equipments')->get();
        
        $equipments = DB::table('Equipments')
        ->join('Companys', 'Equipments.comp_id', '=', 'Companys.comp_id')
        ->join('Rooms', 'Equipments.room_id', '=', 'Rooms.room_id')
        ->join('Departments', 'Equipments.dep_id', '=', 'Departments.dep_id')
        ->select('Equipments.*', 'Companys.comp_name', 'Rooms.room_name', 'Departments.dep_name')
        ->get();
        return view('equipments.index', ['equipments'=>$equipments]);///
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = DepartmentsModel::all();
        $companys = CompanysModel::all();
        $rooms = RoomsModel::all();
        return view('equipments.create', compact('departments','companys','rooms'));//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'eq_name'=> 'required',
            'eq_serialnumber'=> 'required',
            'comp_id'=> 'required',
            'dep_id'=> 'required',
        ]);

        $equipments = new EquipmentsModel;
        $equipments->eq_serialnumber = $request->eq_serialnumber;
        $equipments->eq_name = $request->eq_name;
        $equipments->eq_brand = $request->eq_brand;
        $equipments->eq_status = 'available';
        $equipments->import_date = Carbon::now()->toDateString();
        $equipments->export_date = $request->export_date;
        $equipments->comp_id = $request->comp_id;
        $equipments->dep_id = $request->dep_id;
        $equipments->room_id = $request->room_id;
        $equipments->create_by = $request->user()->name;
        $equipments->update_by = $request->user()->name;
        $equipments->save();

        return redirect('equipments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $equipments = EquipmentsModel::find($id);
        return view('checkequipments.create', ['equipments'=>$equipments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipment = EquipmentsModel::find($id);
        $departments = DepartmentsModel::all();
        $companys = CompanysModel::all();

        return view('equipments.edit', compact('equipment','departments','companys'));//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'eq_name'=> 'required',
            'comp_id'=> 'required',
            'dep_id'=> 'required',
        ]);

        $equipment = EquipmentsModel::find($id);

        $equipment->eq_name = $request->get('eq_name');
        $equipment->eq_brand = $request->get('eq_brand');
        $equipment->export_date = $request->get('export_date');
        $equipment->comp_id = $request->get('comp_id');
        $equipment->dep_id = $request->get('dep_id');
        $equipment->update_by = $request->user()->name;
        $equipment->save();
        return redirect('equipments');//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equipment = EquipmentsModel::find($id);

        $equipment->delete();
        return redirect('equipments');
    }
}

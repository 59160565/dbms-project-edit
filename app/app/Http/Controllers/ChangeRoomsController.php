<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\EquipmentsModel;
use App\RoomsModel;
use App\ChangeRoomsModel;
use Carbon\Carbon;

class ChangeRoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $changerooms = DB::table('ChangeRooms')->get();
        return view('changerooms.index', ['changerooms'=>$changerooms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            DB::table("ChangeRooms")->insert([
                'change_id'=>$request->change_id,
                'changeeq_serialnumber'=>$request->changeeq_serialnumber,
                'oldroom_id'=>$request->oldroom_id,
                'newroom_id'=>$request->newroom_id,
                'create_by'=>$request->user()->name,
                'update_by'=>$request->user()->name,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]);

        } catch(ValidationException $e) {
            DB::rollback();
        }

        DB::commit();

    return redirect('equipments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $equipments = EquipmentsModel::find($id);
        $rooms = RoomsModel::all();
        return view('changerooms.create', ['equipments'=>$equipments,'rooms'=>$rooms]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $changeroom = ChangeRoomsModel::find($id);

        $changeroom->delete();
        return redirect('changerooms');//
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\RoomsModel;
use Carbon\Carbon;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = DB::table('Rooms')->get();
        return view('rooms.index', ['rooms'=>$rooms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            DB::table("Rooms")->insert([
                'room_id'=>$request->room_id,
                'room_name'=>$request->room_name,
                'create_by'=>$request->user()->name,
                'update_by'=>$request->user()->name,
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]);

        } catch(ValidationException $e) {
            DB::rollback();
        }

        DB::commit();

    return redirect('equipments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rooms = RoomsModel::find($id);
        return view('rooms.create', ['rooms'=>$rooms]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rooms = RoomsModel::find($id);

        return view('rooms.edit', compact('rooms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'room_id'=> 'required',
            'room_name'=> 'required',
        ]);

        $rooms = RoomsModel::find($id);

        $rooms->room_id = $request->get('room_id');
        $rooms->room_name = $request->get('room_name');
        $rooms->update_by = $request->user()->name;
        $rooms->save();
        return redirect('rooms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = RoomsModel::find($id);

        $room->delete();
        return redirect('rooms');
    }
}

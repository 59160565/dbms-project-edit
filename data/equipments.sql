DROP TABLE IF EXISTS Users;

DROP TABLE IF EXISTS Equipments; 
DROP TABLE IF EXISTS CheckEquipments;
DROP TABLE IF EXISTS RepairEquipments;
DROP TABLE IF EXISTS LostEquipments;

DROP TABLE IF EXISTS Departments;

DROP TABLE IF EXISTS Rooms;
DROP TABLE IF EXISTS ChangeRooms;

DROP TABLE IF EXISTS Companys;

CREATE TABLE Users (
  user_id INT NOT NULL,
  user_name VARCHAR(100) NOT NULL,
  user_pass VARCHAR(100) NOT NULL,
  create_at DateTime NOT NULL
)

CREATE TABLE Equipments (
  id INT NOT NULL,
  eq_serialnumber VARCHAR(100) NOT NULL UNIQUE,
  eq_name VARCHAR(100) NOT NULL,
  eq_brand VARCHAR(100) ,
  eq_status ENUM('available', 'repair', 'lost') NOT NULL,
  import_date Date NOT NULL,
  export_date Date NOT NULL,
  comp_id VARCHAR(100) NOT NULL,
  dep_id VARCHAR(100) NOT NULL,
  room_id VARCHAR(100) NOT NULL,
  create_by VARCHAR(100) NOT NULL,
  update_by VARCHAR(100) NOT NULL
)

CREATE TABLE CheckEquipments (
  id INT NOT NULL,
  check_id VARCHAR(100) NOT NULL UNIQUE,
  checkeq_serialnumber VARCHAR(100) NOT NULL,
  check_by VARCHAR(100) NOT NULL,
  check_date Date NOT NULL,
  check_status ENUM('available', 'repair', 'lost') NOT NULL
)

CREATE TABLE RepairEquipments (
  id INT NOT NULL,
  repair_id VARCHAR(100) NOT NULL UNIQUE,
  repaircheck_id VARCHAR(100) NOT NULL,
  repaireq_serailnumber VARCHAR(100) NOT NULL,
  request_by VARCHAR(100) NOT NULL,
  request_date Datet NOT NULL
)

CREATE TABLE LostEquipments (
  id INT NOT NULL ,
  lost_id VARCHAR(100) NOT NULL UNIQUE,
  lostcheck_id VARCHAR(100) NOT NULL,
  losteq_serailnumber INT NOT NULL,
  request_by VARCHAR(100) NOT NULL,
  request_date Date NOT NULL
)

CREATE TABLE Departments (
  id INT NOT NULL,
  dep_id VARCHAR(100) NOT NULL UNIQUE,
  dep_name VARCHAR(100) NOT NULL,
  create_by VARCHAR(100) NOT NULL,
  update_by VARCHAR(100) NOT NULL
)

CREATE TABLE Rooms (
  id INT NOT NULL,
  room_id VARCHAR(100) NOT NULL UNIQUE,
  room_name VARCHAR(100) NOT NULL,
  create_by VARCHAR(100) NOT NULL,
  update_by VARCHAR(100) NOT NULL
)

CREATE TABLE ChangeRooms (
  id INT NOT NULL,
  change_id VARCHAR(100) NOT NULL UNIQUE,
  request_by VARCHAR(100) NOT NULL,
  request_date Date NOT NULL,
  changeeq_serialnumber VARCHAR(100) NOT NULL,
  changeoldroom_id VARCHAR(100) NOT NULL,
  changenewroom_id VARCHAR(100) NOT NULL,
  create_by VARCHAR(100) NOT NULL,
  update_by VARCHAR(100) NOT NULL
)

CREATE TABLE Companys (
  id INT NOT NULL,
  comp_id VARCHAR(100) NOT NULL UNIQUE,
  comp_name VARCHAR(100) NOT NULL,
  comp_addr VARCHAR(100) NOT NULL,
  create_by VARCHAR(100) NOT NULL,
  update_by VARCHAR(100) NOT NULL
)
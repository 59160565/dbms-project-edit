DELIMITER $$
DROP TRIGGER IF EXISTS ChangeRoom $$

CREATE TRIGGER ChangeRoom
    AFTER INSERT ON ChangeRooms
    FOR EACH ROW
BEGIN

     UPDATE Equipments SET
        room_id = NEW.newroom_id,
        update_by = NEW.update_by
        WHERE eq_serialnumber = NEW.changeeq_serialnumber;

END $$
DELIMITER ;
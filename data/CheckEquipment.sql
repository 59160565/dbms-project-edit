DELIMITER $$
DROP TRIGGER IF EXISTS CheckEquipment $$

CREATE TRIGGER CheckEquipment
    AFTER INSERT ON CheckEquipments
    FOR EACH ROW
BEGIN

     UPDATE Equipments SET
        eq_status = NEW.check_status,
        update_by = NEW.update_by
        WHERE eq_serialnumber = NEW.checkeq_serialnumber;

    IF (NEW.check_status='lost') THEN
        INSERT INTO LostEquipments (
            lost_id,
            lostcheck_id,
            losteq_serialnumber,
            create_by,
            update_by,
            created_at
        ) VALUES (
            NEW.lr_id,
            NEW.check_id,
            NEW.checkeq_serialnumber,
            NEW.create_by,
            NEW.update_by,
            NEW.created_at
        );
    ELSEIF (NEW.check_status='repair') THEN
        INSERT INTO RepairEquipments (
            repair_id,
            repaircheck_id,
            repaireq_serialnumber,
            update_by,
            create_by,
            created_at
        ) VALUES (
            NEW.lr_id,
            NEW.check_id,
            NEW.checkeq_serialnumber,
            NEW.update_by,
            NEW.create_by,
            NEW.created_at
        );

    END IF;
END $$
DELIMITER ;